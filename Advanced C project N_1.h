// This is the header of Proit.c
// Please check Proit.c file

#include<stdio.h>                      // FOR INPUT AND OUTPUT
#include<stdlib.h>                     // FOR STRING 
#include<string.h>                     // FOR STRUCTURE

int reg(int i);                         // FOR FUNCTION REGISTRATION
int dis(int i, int t);                  // FOR DISPLAY
int dis1(int i, int t, int l); 
int del(int i, int t);                 // FOR DELETE FUNCTION
int find(int i, int t, int l);         // FOR SEARCH FUNCTION

int main()                             // MAIN FUNCTION TO CALL OTHER FUNCS
{
	int n, i=0, t, l;                  // INTS ARE DECLARED HERE TOO
	while(1)                            // TRUE WHILE
	{
		printf("\tRegistration\n");
		printf("1.Enter info:\n2.Show all:\n3.Delete:\n4.Findbybirth:\n");
		printf("5.Exit:\n");
		scanf("%d", &n);              // TO CHOOSE FROM THE MENU
		switch (n)                    // SWITCH IS USED TO CHOOSE
		{
			case 1:
				reg(i); // TO CALL REGISTRATION FUNCTION
				break;
			case 2:
				l=0;                    // IF IT IS TO DISPLAY ALL IF CALLED FROM FIND L IS CHANGED THERE
				dis(i, t); // TO CALL DISPLAY FUNCTION
				break;
			case 3:
				del(i,t); // TO CALL DELETE FUNCTION 
				break;
			case 4:
				find(i,t,l); // TO CALL FIND FUNCTION 
				break;
			case 5:
				exit(0); // TO EXIT FROM THE PROGRAM
			default:     // IF DEFAULT HAPPENS THEN RUNS THIS 
				printf("Please make sure you pressed a right number !");
		}
		if(n==1)
		{                                         // IF THE PROGRAM IS WITHOUT THIS THEN WHEN EVEN 2 IS PRESSED ARRAY KEEPS INCREASING
			i++;                                  // I IS INCREASED
			t=i;                                  // T IS HOW MANY INFO THERE ARE
		}

	}
	return 0; // END
}
