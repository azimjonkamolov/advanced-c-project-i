// Name: Advanced C project 1
// Time: 09:07 11.05.2018
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to store students' info by name, phone number, birth day
// This program consists of two parts

#include"Proit.h"  

struct N                                            // CREATED STRUCTURE WITH NAME N
{
	char name[20];                                 // CHAR ARRAY FOR NAME
	int num;                                       // INT FOR PHONE NUMBER
	int bday;                                     // INT FOR BIRTH DAY
	
}lol[100], temp;                                 // ARRAY TO STORE UP TO 100 AND TEMP HOLDER TO SORT BY NAME

int reg(int i)                                   // FUNCTION FOR REGISTRATION WITH INT I ARGUMENT 
{
	printf("Enter the name please: ");  
	scanf("%s", lol[i].name);                   // TO ENTER A NAME
	printf("Enter the phone number please: ");
	scanf("%d", &lol[i].num);                   // TO ENTER A PHONE NUMBER
	printf("Enter the birth day please: ");
	scanf("%d", &lol[i].bday);                  // TO ENTER A BIRTH DAY
	printf("\n");                              // TO SEPARATE FROM OTHER INFO
}

int dis1(int i, int t, int l)                  // FUNCTION FOR DISPLAY WITH INT I, T, L ARGUMENTS <<FOR SHOW FUNCTION ONLY>>
{
	                                           // MY DISPLAY IS DIFFERENT THAN ASKED, IT IS DONE IN THIS WAY TO LOOK BETTER
	int flag=0;                                // TO CHECK WHETHER THE ARRAY IS EMPTY OR NOT
	for(i=l;i<t;i++)                           // FOR LOOP L WILL BE MORE CLEAR LATER ON
	{
		if(lol[i].num==0)                     // TO CHECK IF THE ARRAY HAS A VALUE IN IT
		{
			flag = 1;                       // IF ARRAY IS EMPTY THEN FLAG IS TURNED INTO 1 DOWN ->
		}
		else                                // IF NOT
		{
			printf("The name: %s\n", lol[i].name);          // DISPLAY NAME BY A GIVEN ORDER
			printf("The phone number: %d\n", lol[i].num);   // DISPLAY PHONE NUMBER BY A GIVEN ORDER
			printf("The birth day: %d\n", lol[i].bday);     // DISPLAY BIRTH DAY BY A GIVEN ORDER
		}
	}	
	if(flag==1)                               // IF THE ARRAY IS EMPTY
	{
        // LEAVES IT EMPTY
	}
}

int dis(int n, int i)                       // DISPLAY FUNCTION FOR ALPHABETIC ORDER
{
	int j, flag=0;                          // INTS J AND FLAG

		for (i=1;i<n;i++)                   // TO STORE AS ALPHABET
			for(j=0;j<n-i;j++)              // TO STORE AS ALPHABET
			{
					if(lol[i].num==0)       // TO CHECK IF THE ARRAY HAS A VALUE IN IT
					{				
						flag=1;             // IF NOT, THEN MAKE FLAG 1
					}
					else                    // IT IT HAS, THEN RUNS THIS
					{
						if(strcmp(lol[j].name,lol[j+1].name)>0) // COMPARE STRINGS
						{
							temp=lol[j];        // CHANGE THEM HERE
							lol[j]=lol[j+1];    // CHANGE THEM HERE
							lol[j+1]=temp;      // CHANGE THEM HERE
						}	
											
					}
			}		
	
	if(flag==1)
	{
    // IF EMPTY, LEAVES IT 
	}
	else // SHOWS THE OUTPUT
	{
		for(i=0;i<n;i++)                                        // TO SHOW THEM 
		{
			printf("*******%d********\n", i+1);                 // TO SHOW THE ORDER
			printf("The name of %d: %s\n", i+1, lol[i].name);   // TO SHOW THE NAME
			printf("The num of %d: %d\n", i+1, lol[i].num);    // TO SHOW THE NUMBER
			printf("The num of %d: %d\n", i+1, lol[i].bday);   // TO SHOW THE BIRTHDAY	
		}
	}
	printf("\n"); // TO MAKE A NEW LINE
}

int find(int i, int t, int l)                           // FUNCTION TO FIND WITH INT I, T, L ARGUMENTS
{
	int f;                                              // INT F FOR SEARCH VALUE
		printf("Enter the birth date: ");
		scanf("%d", &f);                                // ENTER HERE SEARCHING BIRTHDAY 
		for(i=l;i<t;i++)                               // L IS USED TO SHOW ONLY EXACT ONE ARRAY
		{
			if(f==lol[i].bday)                         // TO SEARCH AMONG THE ARRAY
			{
				l=i;                                   // TO START SHOWING FROM L LOCATION
				t=i+1;                                 // MUST BE ONE NUMBER HIGHER OTHERWISE NO OUTPUT
				dis1(i, t, l);                         // CALL DIS FUNCTION HERE
			}
		}
		printf("\n");	                                // TO MAKE A NEW LINE
}

int del(int i, int t)                               // FUNCTION TO DELTE ITEMS IN ARRAY
{
	for(i=0;i<t;i++)                               // RUN LOOP
	{
		lol[i].bday=0;                             // MAKE IT 0
		lol[i].name[i]=0;                          // MAKE IT 0
		lol[i].num=0;                              // MAKE IT 0
	}
}